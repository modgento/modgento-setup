<?php

namespace Modgento\Setup\Setup;

use Magento\Config\Model\ConfigFactory;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;

class ConfigImport
{

    protected $configFactory;

    public function __construct(
        ConfigFactory $configFactory
    ) {
        $this->configFactory = $configFactory;
    }

    public function update(ModuleDataSetupInterface $version, $context)
    {
        if (version_compare($context, '1.0.1', '<')) {
            $this->importSetting('web/unsecure/base_url', 'http://modgento.local/');
        }
    }

    private function importSetting($setting, $value)
    {
        $config = $this->configFactory->create();
        $config->setDataByPath($setting, $value);
        $config->save();
    }
}