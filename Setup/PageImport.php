<?php

namespace Modgento\Setup\Setup;

use Magento\Cms\Model\PageFactory;
use Magento\Framework\Setup\ModuleDataSetupInterface;

class PageImport
{

    protected $source = '/Import/Page/';

    protected $pageFactory;

    public function __construct(
        PageFactory $pageFactory
    ) {
        $this->pageFactory = $pageFactory;
    }

    public function upgrade(ModuleDataSetupInterface $setup, $version)
    {
        if (version_compare($version, '1.0.1','<')) {
            $this->importPage('home', 'Welcome to Modgento, Software and Magento Extensions');
        }
    }

    private function importPage($identifier, $title = null)
    {
        $page = $this->pageFactory->create();

        if (!$page->load($identifier,'identifier')->getId()) {
            if ($title != null) {
                $page->setTitle($title);
            }
            else {
                $page->setTitle(ucfirst($identifier));
            }

            $src = __DIR__ . $this->source . $identifier . '.txt';

            $content = file_get_contents($src);
            $page->setIdentifier($identifier)->setPageLayout('1column')->setContent($content)->setIsActive(true)->setStores([\Magento\Store\Model\Store::DEFAULT_STORE_ID]);
            $page->save();
        }
    }
}
