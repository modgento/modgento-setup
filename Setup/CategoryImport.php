<?php

namespace Modgento\Setup\Setup;

use Magento\Catalog\Model\CategoryFactory;
use Magento\Framework\Setup\ModuleDataSetupInterface;

class CategoryImport
{

    protected $source = '/Import/Category/';

    protected $categoryFactory;

    public function __construct(
        CategoryFactory $categoryFactory
    ) {
        $this->categoryFactory = $categoryFactory;
    }

    public function upgrade(ModuleDataSetupInterface $setup, $version)
    {
        if (version_compare($version, '1.0.1','<')) {
            $this->importCategory('Extensions');
            $this->importCategory('Compose It');
            $this->importCategory('Bespoke Extensions');
            $this->importCategory('Contact');
        }
    }

    private function importCategory($identifier)
    {
        $category = $this->categoryFactory->create();

        if (!$categpry->load($identifier, 'name')->getId()) {
            if ($title != null) {
                $category->setTitle($identifier);
            }

            $file = strtolower(str_replace(' ', '_', $identifier));

            $src = __DIR__ . $this->source . $file . '.txt';

            $content = file_get_contents($src);
            $category->setName($identifier)->setData('description', $content)->setIsActive(true)->setStores([\Magento\Store\Model\Store::DEFAULT_STORE_ID]);
            $category->save();
        }
    }
}
