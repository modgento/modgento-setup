<?php

namespace Modgento\Setup\Setup;

use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\UpgradeDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\InstallSchemaInterface;
use Modgento\Setup\Setup;

class Recurring implements UpgradeDataInterface, InstallSchemaInterface
{

    protected $pageImport;

    protected $configImport;

    protected $setup;

    public function __construct(
        Setup\PageImport $pageImport,
        Setup\ConfigImport $configImport,
        ModuleDataSetupInterface $setup
    ){
        $this->pageImport = $pageImport;
        $this->configImport = $configImport;
        $this->setup = $setup;
    }

    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $version = $context->getVersion();
        $this->pageImport->upgrade($setup, $version);
        $this->configImport->update($setup, $version);
    }

    /**
     * Installs DB schema for a module
     *
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     * @return void
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $this->upgrade($this->setup, $context);
    }
}